@php
    $default_title = seo_default_title();
    $default_description = seo_default_description();
    $default_keywords = seo_default_keywords();

@endphp

<title>@yield('meta_title',$default_title)</title>
<meta name="keywords" content="@yield('meta_keywords',$default_keywords)"/>
<meta name="description" content="@yield('meta_description',$default_description)"/>
@yield('meta_og')