@section('meta_title', $seo->title ?? NULL)
@section('meta_keywords', $seo->keywords?? NULL)
@section('meta_description', $seo->description?? NULL)
@section('meta_og')
@if($seo->opengraph && count($seo->opengraph)>0)
    @foreach ($seo->opengraph as $key=> $og)
        <meta property="og:{{$key}}" content="{{$og}}" />
    @endforeach
@endif
@endsection