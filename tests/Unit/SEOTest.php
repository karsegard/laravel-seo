<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\SEO\Models\SeoRecord;
use KDA\Tests\TestCase;

class SlugTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_seo_record_has_a_title()
    {
        $o = SeoRecord::factory()->create(['title' => 'Fake Title']);
        $this->assertEquals('Fake Title', $o->title);
    }

    /** @test */
    public function a_seo_record_has_a_keyword()
    {
        $o = SeoRecord::factory()->create(['keywords' => 'keyword']);
        $this->assertEquals('keyword', $o->keywords);
    }

    /** @test */
    public function a_seo_record_has_a_description()
    {
        $o = SeoRecord::factory()->create(['description' => 'keyword']);
        $this->assertEquals('keyword', $o->description);
    }
}
