<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Sluggable\Models\Slug;
use KDA\Tests\Models\Post;
use KDA\Tests\Models\PostWithTitle;
use KDA\Tests\TestCase;

class HasSlugTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_post_has_a_seo()
    {
        $p = Post::create([
            'title' => 'Hello world',
        ]);

        $p2 = Post::find($p->id);

        $this->assertNotNull($p2);

        $this->assertNotNull(Post::find($p->id)->indexed);
        $this->assertNotNull(Post::find($p->id)->indexed->title);
        $this->assertNotNull(Post::find($p->id)->indexed->description);
        $this->assertNotNull(Post::find($p->id)->indexed->keywords);
        /*$this->assertEquals($p->slugs->first()->slug, 'hello_world');
        $this->assertEquals($p->slugs->first()->getFullPath(), 'posts/hello_world');*/
    }

    /** @test */
    public function a_post_has_a_seo_and_title()
    {
        $p = PostWithTitle::create([
            'title' => 'Hello world',
        ]);

        $p2 = PostWithTitle::find($p->id);

        $this->assertNotNull($p2);

        $this->assertNotNull($p2->indexed);
        $this->assertEquals($p2->indexed->title, 'Post Hello world');
        $this->assertEquals($p2->indexed->description, 'Describe Hello world');
        $this->assertEquals($p2->indexed->keywords, 'hello,world');
        /*$this->assertEquals($p->slugs->first()->slug, 'hello_world');
        $this->assertEquals($p->slugs->first()->getFullPath(), 'posts/hello_world');*/
    }
}
