<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\SEO\Contracts\GeneratesSEOContent;

class PostWithTitle extends Model implements GeneratesSEOContent
{
    use \KDA\SEO\Models\Traits\HasSeo;
    use HasFactory;

    protected $table = 'posts';

    protected $fillable = [
        'title',
    ];

    public function getSeoTitle(Model $model): string
    {
        return 'Post '.$this->title;
    }

    public function getSeoDescription(Model $model): string
    {
        return 'Describe '.$this->title;
    }

    public function getSeoKeywords(Model $model): string
    {
        return collect(explode(' ', $this->title))->map(function ($item) {
            return trim(strtolower($item));
        })->join(',');
    }


    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }
}
