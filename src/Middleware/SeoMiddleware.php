<?php

namespace KDA\SEO\Middleware;

use Closure;
use KDA\SEO\Models\SeoRoute;

class SeoMiddleware
{
    public function handle($request, Closure $next)
    {
        $route = \Route::current()->getName();
        if (! empty($route)) {
            SeoRoute::firstOrCreate(['name' => $route]);
        }

        return $next($request);
    }
}
