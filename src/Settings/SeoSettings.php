<?php

namespace KDA\SEO\Settings;

use Spatie\LaravelSettings\Settings;

class SeoSettings extends Settings
{
    public string $default_title;

    public string $default_keywords;

    public string $default_description;

    public static function group(): string
    {
        return 'general';
    }
}
