<?php

namespace KDA\SEO\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use KDA\SEO\Facades\SEO;

class GenerateSeo extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:seo:generate {model} {--force} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    public function __construct(Filesystem $files)
    {
        parent::__construct();
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $model = $this->argument('model');
        $force = $this->option('force');

        $validModel = in_array("KDA\SEO\Models\Traits\HasSeo", class_uses_recursive($model));
        if ($validModel) {
            if ($force) {
                SEO::clearForModelType($model);
            }
            $r = $model::withNoSeo()->get();

            foreach ($r as $item) {
                SEO::createForModel($item);
            }
        } else {
            $this->error($model.' doesn\'t use SEO trait');
        }
    }
}
