<?php

namespace KDA\SEO\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class InstallCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:seo:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install kda-seo';

    public function __construct(Filesystem $files)
    {
        parent::__construct();
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->call('vendor:publish', ['--provider' => "KDA\SEO\ServiceProvider", '--tag' => 'config']);
        if ($this->confirm('Install spatie settings now ? ')) {
            $this->call('vendor:publish', ['--provider' => "Spatie\LaravelSettings\LaravelSettingsServiceProvider", '--tag' => 'migrations']);
            $this->call('vendor:publish', ['--provider' => "Spatie\LaravelSettings\LaravelSettingsServiceProvider", '--tag' => 'settings']);

        }
        sleep(2); // avoid collision with spatie settings
        $this->call('vendor:publish', ['--provider' => "KDA\SEO\ServiceProvider", '--tag' => 'settings-migrations']);

        if ($this->confirm('Migrate now ? ')) {
            $this->call('migrate');
        }
    }
}
