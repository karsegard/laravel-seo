<?php

namespace KDA\SEO\View\Components;

use Illuminate\View\Component;

class Seo extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $seo;

    public function __construct($seo)
    {
        //
        $this->seo = $seo;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('kda-seo::components.seo');
    }
}
