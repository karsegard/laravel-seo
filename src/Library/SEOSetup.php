<?php

namespace KDA\SEO\Library;

use KDA\SEO\Contracts\DefaultContent;

class SEOSetup
{
    public static function getDefaultContentUsing($callback)
    {
        app()->singleton(DefaultContent::class, $callback);
    }
}
