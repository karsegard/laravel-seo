<?php

namespace KDA\SEO\Library;

use Closure;
use Illuminate\Database\Eloquent\Model;
use KDA\SEO\Contracts\DefaultContent;
use KDA\SEO\Contracts\GeneratesOGContent;
use KDA\SEO\Contracts\GeneratesSEOContent;
use KDA\SEO\Models\SeoRecord;

class SEOManager
{
    protected $keywords = '';
    protected $title = '';

    protected $openGraphGenerators = [];

    public function generateOpenGraph($generator,Model $model,$values):array{
        $keys = $generator->generateOpenGraphKeys($model);
        $results = [];
        foreach ($keys as $key){
            $results[$key]= $generator->generateOpenGraph($model,$key,array_merge($values,$results));
        }
        return $results;
    }

    public function registerOpenGraphGenerator(string $generator,string|null $model_class){
        $_generator = !blank($model_class) ? ['generator'=>$generator,'model'=>$model_class] : $generator;
        $this->openGraphGenerators[]=$_generator;
    }
    
    public function createRecord (Model $model){
        if(method_exists($item,'shouldGenerateSeo') && $item->shouldGenerateSeo() === false){
            return;
        }
        $defaultContent = app()->make(DefaultContent::class);
        $meta = [
            'title'=>'',
            'keywords'=>'',
            'description'=>''
        ];
        if ($model instanceof GeneratesSEOContent) {
            $meta['title'] = $model->getSeoTitle($model);
            $meta['description'] = $model->getSeoDescription($model);
            $meta['keywords'] = $model->getSeoKeywords($model);
        }
        
        $og = [] ;

        if ($model instanceof GeneratesOGContent) {
            $og = $this->generateOpenGraph($model,$model,$meta);
        }
        

        $defaultTitle = $defaultContent->getDefaultTitle($model);
        $defaultKeywords = $defaultContent->getDefaultKeywords($model);

        if (! empty($defaultTitle)) {
            $meta['title'] = $defaultTitle.' '.$meta['title'];
        }

        if (! empty($defaultKeywords)) {
            $meta['keywords'] = $defaultKeywords.' - '. $meta['keywords'];
        }
        $meta['opengraph']=$og;
        return $meta;
    }
    public function createForModel(Model $model)
    {
        $meta = $this->createRecord($model);        
        $r = SeoRecord::create([
            ...$meta,
            'indexed_id' => $model->id,
            'indexed_type' => get_class($model),
        ]);
    }

    public function updateForModel(Model $model,$force=false)
    {
        $meta = $this->createRecord($model);        
        $r = SeoRecord::where('indexed_id' , $model->id)->where( 'indexed_type' , get_class($model))->first();
        if(!$r->touched || $force ===true){
            $r->update($meta);
        }

    }

    public function deleteForModel($item)
    {
        SeoRecord::where('indexed_type', get_class($item))->where('id', $item->id)->delete();
    }

    public function clearForModelType($item)
    {
        SeoRecord::where('indexed_type', $item)->where('touched',false)->delete();
        SeoRecord::withTrashed()->where('indexed_type', $item)->where('touched',false)->forceDelete();
    }

    public function setDefaultKeywords($keywords)
    {
        return $this->keywords = $keywords;
    }

    public function setDefaultTitle($title)
    {
        return $this->title = $title;
    }
}
