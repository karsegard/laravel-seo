<?php

namespace KDA\SEO\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeoRecord extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'indexed_id',
        'indexed_type',
        'locale',
        'description',
        'title',
        'keywords',
        'meta',
        'validated',
        'touched',
        'opengraph'
    ];

    protected $casts = [
        'meta' => 'array',
        'validated' => 'boolean',
        'touched' => 'boolean',
        'opengraph'=>'array'
    ];

    public function indexed()
    {
        return $this->morphTo();
    }

    public function getIndexableAttribute()
    {
        return $this->indexed ? $this->indexed->indexable : null;
    }

    protected static function newFactory()
    {
        return  \KDA\SEO\Database\Factories\SeoRecordFactory::new();
    }

    public function getGeneratedAttribute()
    {
        return ! $this->touched;
    }
}
