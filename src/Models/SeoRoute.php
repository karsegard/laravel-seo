<?php

namespace KDA\SEO\Models;

use Illuminate\Database\Eloquent\Model;
use KDA\SEO\Facades\SEO;

class SeoRoute extends Model
{
    use Traits\HasSeo;

    protected $fillable = [
        'name',
    ];

    //Seo
    public function getIndexableAttribute()
    {
        return '(Route) '.$this->name;
    }

    public function scopeForCurrentRoute($q)
    {
        return $q->where('name', \Route::current()->getName());
    }
}
