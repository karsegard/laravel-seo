<?php

namespace KDA\SEO\Models\Traits;

use KDA\SEO\Facades\SEO;
use KDA\SEO\Models\SeoRecord;

trait HasSeo
{
    public static function bootHasSeo()
    {
        static::created(function ($item) {
            SEO::createForModel($item);
        });

        static::updated(function ($item) {
            SEO::updateForModel($item);
        });

        static::deleting(function ($item) {
            SEO::deleteForModel($item);
        });
    }

    public function indexed()
    {
        return $this->morphOne(SeoRecord::class, 'indexed');
    }

    public function scopeWithNoSeo($query)
    {
        return $query->whereDoesntHave('indexed');
    }

    public function getSeoDescriptionAttribute()
    {
        if ($this->indexed) {
            return $this->indexed->description;
        }

        return null;
    }

    public function getSeoTitleAttribute()
    {
        if ($this->indexed) {
            return $this->indexed->title;
        }

        return null;
    }
}
