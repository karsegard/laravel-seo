<?php

namespace KDA\SEO\Contracts;

use Illuminate\Database\Eloquent\Model;

interface DefaultContent
{
    public function getDefaultKeywords(Model $model);

    public function getDefaultTitle(Model $model);
}
