<?php

namespace KDA\SEO\Contracts;

use Illuminate\Database\Eloquent\Model;

interface GeneratesOGContent
{
 
    public function generateOpenGraphKeys(Model $model): array;
    public function generateOpenGraph(Model $model, string $key,$values):string;

}
