<?php

namespace KDA\SEO\Contracts;

use Illuminate\Database\Eloquent\Model;

interface GeneratesSEOContent
{
    public function getSeoTitle(Model $model): string;

    public function getSeoDescription(Model $model): string;

    public function getSeoKeywords(Model $model): string;

  //  public function generateOpenGraphMetaKeys(Model $model): array;
}
