<?php

namespace KDA\SEO;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasHelper;
use KDA\SEO\Actions\DefaultContent as DefaultContentAction;
use KDA\SEO\Library\SEOSetup;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasDumps;
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasComponents;
    use \KDA\Laravel\Traits\HasSpatieSettings;
    use HasHelper;

    protected $packageName = 'kda-seo';

    protected $spatieSettingsClass = [
        KDA\SEO\Settings\SeoSettings::class,
    ];

    protected $components = [
        'seo' => View\Components\Seo::class,
    ];

    protected $registerViews = 'kda-seo';

    protected $publishViewsTo = 'vendor/kda/seo';

    protected $configs = [
        'kda/seo.php' => 'kda.seo',
    ];

    protected $dumps = [
        'seo_records',
    ];

    protected $_commands = [
        Commands\GenerateSeo::class,
        Commands\InstallCommand::class,
    ];

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function register()
    {
        parent::register();
        $this->app->singleton('kda.seomanager', function ($app) {
            return new Library\SEOManager();
        });
        SEOSetup::getDefaultContentUsing(DefaultContentAction::class);
    }
}
