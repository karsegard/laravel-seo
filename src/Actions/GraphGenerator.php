<?php

namespace KDA\SEO\Actions;

use Illuminate\Database\Eloquent\Model;
use KDA\SEO\Contracts\GeneratesOGContent;

class GraphGenerator implements GeneratesOGContent
{
   
    public function generateOpenGraphKeys(Model $model): array{
        return [
            
        ];
    }

    public function generateOpenGraph(Model $model, string $key):string{
        return '';
    }

}
