<?php

namespace KDA\SEO\Actions;

use Illuminate\Database\Eloquent\Model;
use KDA\SEO\Contracts\DefaultContent as DefaultContentContract;

class DefaultContent implements DefaultContentContract
{
    public function getDefaultKeywords(Model $model)
    {
        return '';
    }

    public function getDefaultTitle(Model $model)
    {
        return '';
    }
}
