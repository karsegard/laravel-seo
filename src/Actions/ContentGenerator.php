<?php

namespace KDA\SEO\Actions;

use Illuminate\Database\Eloquent\Model;
use KDA\SEO\Contracts\GeneratesSEOContent;

class ContentGenerator implements GeneratesSEOContent
{
    public function getSeoTitle(Model $model): string
    {
        return '';
    }

    public function getSeoDescription(Model $model): string
    {
        return '';
    }

    public function getSeoKeywords(Model $model): string
    {
        return '';
    }

   /* public function generateOpenGraphMetaKeys(): array
    {
        return [];
    }*/
}
