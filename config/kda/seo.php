<?php

return [
    'default_title' => 'You should define a default title',
    'default_description' => 'You should define a default description',
    'default_keywords' => 'default, keywords',

];
