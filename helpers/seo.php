<?php

use KDA\SEO\Settings\SeoSettings;

if (! function_exists('seo_default_title')) {
    function seo_default_title()
    {
        return app(SeoSettings::class)->default_title;
    }
}

if (! function_exists('seo_default_keywords')) {
    function seo_default_keywords()
    {
        return app(SeoSettings::class)->default_keywords;
    }
}

if (! function_exists('seo_default_description')) {
    function seo_default_description()
    {
        return app(SeoSettings::class)->default_description;
    }
}
