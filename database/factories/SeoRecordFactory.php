<?php

namespace KDA\SEO\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\SEO\Models\SeoRecord;

class SeoRecordFactory extends Factory
{
    protected $model = SeoRecord::class;

    public function definition()
    {
        return [

        ];
    }
}
