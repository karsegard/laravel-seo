<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('seo_records', function (Blueprint $table) {
            $table->id();
            $table->nullableNumericMorphs('indexed');
            $table->string('locale')->default('');
            $table->string('description')->nullable();
            $table->string('title')->nullable();
            $table->string('keywords')->nullable();
            $table->boolean('validated')->default(false);
            $table->text('meta')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_records');
    }
}
