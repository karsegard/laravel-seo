<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateSeoSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('general.default_title', 'Please set up a default title');
        $this->migrator->add('general.default_keywords', 'website');
        $this->migrator->add('general.default_description', 'My website');
    }
}
